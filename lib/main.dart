import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hello World',
      home: Scaffold(
        appBar: AppBar(title: Text('Hello World')),
        body: Center(
          child: Text('''
          Chihiro Nishijima
          Spring 2021
          Work hard in silence. Let success make the noise. '''),
        ),
      ),
    );
  }
}
